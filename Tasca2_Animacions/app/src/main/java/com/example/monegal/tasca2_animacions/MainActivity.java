package com.example.monegal.tasca2_animacions;

import android.animation.AnimatorListenerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ImageView imatge;
    private Animation anim;

    private SeekBar speedControl = null;
    private int speed=1;

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imatge = (ImageView) findViewById(R.id.imageView);

        textView = (TextView)findViewById(R.id.text);

        //seekBar
        speedControl = (SeekBar) findViewById(R.id.seekBar);

        speedControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                if(progress==0){
                    progress=1;
                }
                speed=progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(MainActivity.this,"seek bar progress:"+speed,
                        Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void listener(Animation anim){
        anim.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation a) {
                textView.setText("runing");
            }
            public void onAnimationRepeat(Animation a) {}
            public void onAnimationEnd(Animation a) {
                textView.setText("stop");
            }
        });
    }

    public void fade_out(View v) {
        anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_out);
        anim.setDuration(30000/speed);
        listener(anim);
        imatge.startAnimation(anim);
    }

    public void fade_in(View v) {
        anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_in);
        anim.setDuration(30000/speed);
        listener(anim);
        imatge.startAnimation(anim);
    }

    public void zoomIn(View v) {
        anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.zoom_in);
        anim.setDuration(30000/speed);
        listener(anim);
        imatge.startAnimation(anim);
    }

    public void zoomOut(View v) {
        anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.zoom_out);
        anim.setDuration(30000/speed);
        listener(anim);
        imatge.startAnimation(anim);
    }

    public void topBot(View v) {
        anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.leftright);
        anim.setDuration(30000/speed);
        listener(anim);
        imatge.startAnimation(anim);
    }

    public void leftRight(View v) {
        anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.topbottom);
        anim.setDuration(30000/speed);
        listener(anim);
        imatge.startAnimation(anim);
    }

    public void bounce(View v) {
        anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.bounce);
        anim.setDuration(30000/speed);
        listener(anim);
        imatge.startAnimation(anim);
    }

    public void flash(View v){
        anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.flash);
        anim.setDuration(30000/speed);
        listener(anim);
        imatge.startAnimation(anim);
    }


    public void rotate(View v){
        anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate);
        anim.setDuration(30000/speed);;
        listener(anim);
        imatge.startAnimation(anim);
    }


    public void several(View v){
        anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.several);
        anim.setDuration(30000/speed);;
        listener(anim);
        imatge.startAnimation(anim);

    }


}
