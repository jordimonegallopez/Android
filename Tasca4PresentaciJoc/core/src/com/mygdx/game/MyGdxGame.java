package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;


public class MyGdxGame extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;
	int x = 0;
	int y = 0;
	int width;
	int height;
	OrthographicCamera cam;
	Stage stage;
	Sprite personatge;
	int amplepersona =43;
	int altpersona = 64;
	int pas = 0;
	int canvipas=0;
	int sentit=0;
	Texture textura;
	int augmentarPassos = 10;
	int velocitat = 0;
	int contador = 0;
	int punts = 0;
	BitmapFont font ;
	Sound sound;
	@Override
	public void create () {
		sound = Gdx.audio.newSound(Gdx.files.internal("r1.mp3"));
		sound.play(1.0f);
		font = new BitmapFont();
		width = Gdx.graphics.getWidth();
		height = Gdx.graphics.getHeight();
		cam = new OrthographicCamera(width, height );
		cam.setToOrtho(false, width, height);
		batch = new SpriteBatch();
		textura = new Texture("caminador.png");
		textura.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Linear );
		personatge = new Sprite(textura); //Creem un personatge i li assignem la textura.
		personatge.setSize (amplepersona, altpersona);
		personatge.setRegion(pas*amplepersona, altpersona*sentit, (pas+1)*amplepersona,
				altpersona);
		personatge.setPosition(10, 50);

	}

	@Override
	public void render () {

		canvipas = (canvipas + 1) % augmentarPassos;
		if (canvipas == 0) {
			pas = (pas + 1) % 6;
			personatge.setRegion(pas * amplepersona, altpersona * sentit, amplepersona,
					altpersona);
			contador ++;
			if(contador == 3) {
				punts++;
				contador = 0;
			}
		}

		if(x <= width && sentit == 0){
			x = x + 1 + velocitat;
		}else if(x>= 0){
			x = x - 1 - velocitat;
			augmentarPassos = 5;
		}else {
			sentit = 0;
		}
		if(x >= width || x <= 0){
			velocitat++;
		}

        if(x>=0){
            sentit = 1;
        }
		Gdx.gl20.glClearColor(1, 1, 1, 1);
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		batch.draw(personatge, x, 10);
		font.draw(batch, "Passos: " + punts + "", 100,100);
		batch.end();

	}


	@Override
	public void dispose () {
		batch.dispose();
		sound.dispose();
	}
}
