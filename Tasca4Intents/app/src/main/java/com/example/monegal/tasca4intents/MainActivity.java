package com.example.monegal.tasca4intents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void changeActivity(View view)
    {
        Intent intent = new Intent(this, Main2Activity.class);
        startActivityForResult(intent,1);
    }

    @Override
    protected void onActivityResult(int resquestCode, int resultCode, Intent data){

            Log.d("fff",Integer.toString(data.getIntExtra("position",0)));

            text = (TextView) findViewById(R.id.textView2);
            text.setText(String.valueOf(""));

            if(data.getIntExtra("position",0)==0) {
                text = (TextView) findViewById(R.id.textView2);
                text.setText(String.valueOf("web"));
            }

            if(data.getIntExtra("position",0)==1) {
                text = (TextView) findViewById(R.id.textView2);
                text.setText(String.valueOf("telefon"));
            }

            if(data.getIntExtra("position",0)==2) {
                text = (TextView) findViewById(R.id.textView2);
                text.setText(String.valueOf("show map"));
            }

            if(data.getIntExtra("position",0)==3) {
                text = (TextView) findViewById(R.id.textView2);
                text.setText(String.valueOf("search on map"));
            }

            if(data.getIntExtra("position",0)==4) {
                text = (TextView) findViewById(R.id.textView2);
                text.setText(String.valueOf("take a picture"));
            }

            if(data.getIntExtra("position",0)==5) {
                text = (TextView) findViewById(R.id.textView2);
                text.setText(String.valueOf("show contacts"));
            }

            if(data.getIntExtra("position",0)==6) {
                text = (TextView) findViewById(R.id.textView2);
                text.setText(String.valueOf("edit first contact"));
            }

    }

}
