package com.example.monegal.tasca_1_programaciomultimedia;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    private SoundPool sp;
    private int idFX1;
    private int idFX2;
    private int nowPlaying;
    private SeekBar volumeControl = null;
    private int progressChanged;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                 AudioAttributes audioAttributes = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                         .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).build();
            sp=new SoundPool.Builder().setMaxStreams(5).setAudioAttributes(audioAttributes).build();
        }else{
            sp= new SoundPool(5, AudioManager.STREAM_MUSIC,0);
        }



        //seekBar
        volumeControl = (SeekBar) findViewById(R.id.seekBar);

        volumeControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                progressChanged=progress;
                sp.setVolume(nowPlaying,progressChanged/100f,progressChanged/100f);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(MainActivity.this,"seek bar progress:"+progressChanged,
                        Toast.LENGTH_SHORT).show();
            }
        });

        try{
            AssetManager assetManager = this.getAssets();
            AssetFileDescriptor descriptor;

            descriptor = assetManager.openFd("so1.mp3");
            idFX1 = sp.load(descriptor,0);
        }catch(IOException e){
            Log.e("error","failed to load sound files");
        }

        try{
            AssetManager assetManager = this.getAssets();
            AssetFileDescriptor descriptor;

            descriptor = assetManager.openFd("so2.mp3");
            idFX2 = sp.load(descriptor,0);
        }catch(IOException e){
            Log.e("error","failed to load sound files");
        }

    }


    public void so1(View v)
    {
         nowPlaying = sp.play(idFX1, progressChanged,progressChanged, 0, 1,1);
    }

    public void stop(View v)
    {
       sp.stop(nowPlaying);

    }

    public void so2(View v)
    {
        nowPlaying = sp.play(idFX2, progressChanged,progressChanged, 0, 1,1);
    }




}
