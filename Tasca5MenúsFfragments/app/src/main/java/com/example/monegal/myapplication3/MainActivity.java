package com.example.monegal.myapplication3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    fragmentOne imatge;
    fragmentTwo llista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imatge = new fragmentOne();
        llista = new fragmentTwo();
        getSupportFragmentManager().beginTransaction().add(R.id.frameLayout,imatge).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch(item.getItemId()){
            case R.id.item1:
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout,imatge).commit();
                return true;
            case R.id.item2:
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout,llista).commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }





}
