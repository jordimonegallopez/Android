package com.example.jordi.myapplication;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int txtSize = 14;
    private TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void suma(View v){
         text = (TextView) findViewById(R.id.textView2);
        text.setText(String.valueOf(Integer.parseInt(text.getText().toString())+1));
    }

    public void resta(View v){
         text = (TextView) findViewById(R.id.textView2);
        text.setText(String.valueOf(Integer.parseInt(text.getText().toString())-1));
    }

    public void gran(View e){
        text= (TextView) findViewById(R.id.textView2);
        txtSize++;
        text.setTextSize(txtSize);
    }


    public void petit(View e){
        text = (TextView) findViewById(R.id.textView2);
        txtSize--;
        text.setTextSize(txtSize);
    }

    public void color(View e){
        text = (TextView) findViewById(R.id.textView2);
        text.setTextColor(Color.BLUE);
    }


    public void backgroundColor(View v) {
        Button t = (Button) findViewById(R.id.button4);
        t.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        t.setTextColor(Color.RED);
    }

    public void amagar(View v){
        text = (TextView) findViewById(R.id.textView2);
        text.setVisibility(View.INVISIBLE);
    }

    public void mostrar(View v){
        text = (TextView) findViewById(R.id.textView2);
        text.setVisibility(View.VISIBLE);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putFloat("valorTextMida",text.getTextSize());
        outState.putInt("valorColor",text.getCurrentTextColor());
        outState.putCharSequence("valorText",text.getText());
        outState.putInt("valorHide",text.getVisibility());
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState ) {
        super.onRestoreInstanceState(savedInstanceState);
        text.setTextSize(savedInstanceState.getFloat("valorTextMida"));
        text.setTextColor(savedInstanceState.getInt("valorColor"));
        text.setText(savedInstanceState.getCharSequence("valorText"));
        text.setVisibility(savedInstanceState.getInt("valorHide"));
    }
}
